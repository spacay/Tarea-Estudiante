package com.example.demo.Visual;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Edgar Gómez on 15/07/2017.
 */
@SpringUI
public class App extends UI {

    @Autowired
    MyRepository estudianteRepository;

    @Override

    protected void init(VaadinRequest vaadinRequest) {
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();
        TextField nombre = new TextField("Nombre");
        TextField edad = new TextField("Edad");

        Grid<Estudiante> grid = new Grid<>();
        grid.addColumn(Estudiante::getId).setCaption("Id");
        grid.addColumn(Estudiante::getNombre).setCaption("Nombre");
        grid.addColumn(Estudiante::getEdad).setCaption("Edad");

        Button add = new Button("Adicionar");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Estudiante e = new Estudiante();
                e.setNombre(nombre.getValue());
                e.setEdad(Integer.parseInt(edad.getValue()));
                estudianteRepository.save(e);
                grid.setItems(estudianteRepository.findAll());

                nombre.clear();
                edad.clear();
            }
        });
        hlayout.addComponents(nombre, edad, add);
        hlayout.setComponentAlignment(add, Alignment.BOTTOM_RIGHT);
        layout.addComponent(hlayout);
        layout.addComponent(grid);
        setContent(layout);
    }
}
